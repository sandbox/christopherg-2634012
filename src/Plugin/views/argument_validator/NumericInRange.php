<?php

/**
 * @file
 * Contains \Drupal\views_numeric_range_validator\Plugin\views\argument_validator\NumericInRange.
 */

namespace Drupal\views_numeric_range_validator\Plugin\views\argument_validator;

use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Plugin\views\argument_validator\NumericArgumentValidator;

/**
 * Validate whether an argument is numeric and within a configurable range.
 *
 * @ingroup views_argument_validate_plugins
 *
 * @ViewsArgumentValidator(
 *   id = "numeric_in_range",
 *   title = @Translation("Numeric in Range")
 * )
 */
class NumericInRange extends NumericArgumentValidator {

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = parent::defineOptions();

    $options['range_min'] = array('default' => 0);
    $options['range_max'] = array('default' => 1000);

    return $options;
  }

  /**
   * {@inheritdoc}
   */
  public function buildOptionsForm(&$form, FormStateInterface $form_state) {
    parent::buildOptionsForm($form, $form_state);

    $form['range_min'] = array(
      '#type' => 'number',
      '#title' => t('Min'),
      '#default_value' => $this->options['range_min'],
    );
    $form['range_max'] = array(
      '#type' => 'number',
      '#title' => t('Max'),
      '#default_value' => $this->options['range_max'],
    );
  }

  /**
   * {@inheritdoc}
   */
  public function validateArgument($argument) {
    // Use the argument validator from NumericArgumentValidator instead of
    // writing our own numeric validator.
    if (parent::validateArgument($argument) === FALSE) {
      return FALSE;
    }
    elseif ($argument < $this->options['range_min']) {
      return FALSE;
    }
    elseif ($this->options['range_max'] < $argument) {
      return FALSE;
    }

    return TRUE;
  }

}
